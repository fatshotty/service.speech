import os
import json, string, random
import re
import logging
import sys

logger = logging

logger.basicConfig(filename='./speech.log', filemode='a')

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/path/to/credentials.json"

if __name__ != "__main__":
  import xbmcaddon
  ADDON = xbmcaddon.Addon()

  # bypass fucking import-management
  sys.path.append( os.path.join( ADDON.getAddonInfo('path'),'lib') )


import dialogflow_v2 as dialogflow
import speech_recognition as sr


# Generate a hased session id
# Thanks to maxxam for the code
def generate_session_id():
  logger.debug("Generate a new session id")
  s = list(string.ascii_letters + string.digits)
  random.shuffle(s)
  return s[:20]



class ListenRecognizer():

  def __init__(self):
    self.Rec = sr.Recognizer()
    self.Mic = sr.Microphone()


  def check_words(self, text, words):
    text = " {} ".format(text)

    matches = re.search( "\s{}\s".format(words), text, re.IGNORECASE )

    if (matches):
      return True
    else:
      return False


  def listen_and_recognize(self, timeout):

    with self.Mic as source:
      logger.info("listening to microphone with {} timeout".format(timeout) )
      self.Rec.adjust_for_ambient_noise(source)
      audio = self.Rec.listen(source, timeout=timeout or None)

    logger.debug("got an audio, try to recognize")

    try:
      result = self.Rec.recognize_google(audio, language='it-IT')
    except sr.UnknownValueError:
      logger.info("No recognized sentence")
      return None
    except sr.RequestError as e:
      logger.info("request error")
      return None

    logger.info( "Recognized: {}".format(result) )

    return result




  def checkSentences(self, queryText, session_id = None ):
    session_client = dialogflow.SessionsClient()

    if session_id == None:
      session_id = generate_session_id()
      session_id = ''.join(session_id)

    logger.info( 'Session id: {}\n'.format(session_id) )

    session = session_client.session_path('newagent-grgphl', session_id )
    logger.info('Session path: {}\n'.format(session))

    text_input = dialogflow.types.TextInput( text=queryText, language_code='it' )

    query_input = dialogflow.types.QueryInput(text=text_input)

    response = session_client.detect_intent( session=session, query_input=query_input )

    return response

  def stop(self):
    logger.info("Terminate class")
    self.Rec = None
    self.Mic = None





if __name__ == "__main__":
  ListRec = ListenRecognizer()

  while True:

    try:
      print("listening")
      listened = ListRec.listen_and_recognize(1)
      print( "listened {}".format(listened) )

      if listened != None:

        found_activation = ListRec.check_words(listened, "(kodi|codi|kody|cody)")

        if ( found_activation ):

          print("activation work found!")

          while True:
            print("listening for commands")
            listened = ListRec.listen_and_recognize(4)

            print("listened {}".format(listened) )

            response = ListRec.checkSentences( listened )

            print("got response" )

            queryResult = response.query_result
            action = queryResult.action

            if ( action == "input_unknown" or action == "input_sorry" ):
              print( "invalid sentence {}".format(action) )
            else:
              print( "found response {}".format(response) )
              break

            if ( action == "input_sorry" ):
              time.sleep(1)
              break

      else:
        print("not an action recognized")

    except KeyboardInterrupt:
      print("stopped")
      break
    except sr.WaitTimeoutError:
      print("timeout error, repeat")


