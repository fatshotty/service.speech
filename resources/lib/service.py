# -*- coding: utf-8 -*-

"""
=== Flow ===
(every 1 secs, it will be listening for hot words)
-> Ehi Kodi!
<- (image appears on screen)
(wait for 4secs sentence)
-> Val a Rai 1
<- (show message) "ok vado al canale Rai 1"
<- Kodi plays Rai 1 (based on PVR client)

====== OR =====
...
(wait for 4secs sentence)
-> No niente scusa!
<- (show message) "a disposizione :)"
<- (stop listening and re-start flow, see above)
"""

from resources.lib import kodiutils
from resources.lib import kodilogging
import logging
import time
import xbmc
import xbmcaddon
import xbmcgui
import os
import time
import sys

logging.basicConfig(filename=os.path.join(ADDON.getAddonInfo('path'), "speech.log"), filemode='a')

ADDON = xbmcaddon.Addon()
logger = logging

__ACTIVATION_WORD__ = "(kodi|codi|kody|cody)"
__RESOURCES_PATH__   = os.path.join( ADDON.getAddonInfo('path'),'resources')

__PVR_ACTIONS__ = "pvr_actions"
__BASE_NAVIGATION__ = "base_navigation"
__PLAYER_COMMANDS__ = "player_commands"

ListRec = None

monitor = xbmc.Monitor()

def show_message_on_screen(text, timeout=None, nitification=False):
  notification("Speech", text, time=timeout, icon=None, sound=False)


def manage_action(response):

  action = response.query_result.action

  logger.info( "try to handle action: {}".format(action) )

  if not monitor.abortRequested():

    if ( action ==  __PVR_ACTIONS__ ):

      kodi_pvr_actions(response)

    elif ( action == __BASE_NAVIGATION__ ):

      kodi_navigation(response)

    elif (action == __PLAYER_COMMANDS__ ):

      kodi_player_commands(response)

    else:
      show_message_on_screen("Non è prevista nessuna azione per questo comando, mi spiace", None, True)


  else:
    logger.error("abort requested")


  return "ok"


"""
Example: just say:
- Vai a Rai 1
- Metti Rai Due
"""
def kodi_pvr_actions(response):
  # managing action

  logger.info("PVR handle")

  channel = response.query_result.parameters.channel

  # get channels list
  json_resp = kodi_json_request({ "jsonrpc": "2.0", "method": "PVR.GetChannels", "params": {"channelgroupid": "alltv", "properties" :["uniqueid", "channelnumber", "channel"] }, "id": 1} )

  channels = json_resp["result"]

  chl_found = False

  if channels:
    # we have channels list, loop for them
    for chl in channels:
      id = chl["channelid"]
      number = chl["channelnumber"]
      name = chl["label"] # use also 'channel' field
      if id == number or (name.lower().find(channel.lower()) > -1):
        # channel found: play it
        chl_found = True
        kodi_json_request({ "jsonrpc": "2.0", "method": "Player.Open", "params": { "item": { "channelid": id} }, "id": 1 })


  if not chl_found:
    show_message_on_screen("canale {} non trovato, mi spiace".format(channel), None, True)


  return chl_found



def kodi_navigation(response):
  # managing action
  logger.info("NAVIGATION handle")

  # TODO: navigation handler: moveUp - moveDown - moveLeft - moveRighe - pageDown - pageUp - ok - back ...

  return True



def kodi_player_commands(response):
  # managing action

  logger.info("PLAYER handle")

  action = response.query_result.parameters.action
  # TODO: send player commands: play - pause - avanti - indietro - stop

  return True



def start_listening_for_command():
  # Start listening for commands

  # show Microphone to video

  logger.debug("Create the assistant image")

  screen_height = xbmcgui.getScreenHeight()

  # create a 100x69 image positioned to bottom-left corner
  mic_image = xbmcgui.ControlImage(10, screen_height - 100 - 10, 100, 69)
  mic_image.setImage( os.path.join(__RESOURCES_PATH__, "assistant.png") )

  logger.debug("show the assistant image")

  current_window = xbmcgui.Window( xbmcgui.getCurrentWindowId() )
  current_window.addControl( mic_image )


  while True:
    logger.info("listening for command")

    try:
      listened = ListRec.listen_and_recognize(4)

      show_message_on_screen( listened )

      response = ListRec.checkSentences( listened )

      queryResult = response.query_result
      action = queryResult.action

      show_message_on_screen( queryResult.fulfillmentText, 3 )

      if ( action == "input_unknown" or action == "input_sorry" ):
        logger.warn( "invalid sentence {}".format(action) )
      else:
        manage_action( response )

      if ( action == "input_sorry" ):
        time.sleep(1)
        break

    except WaitTimeoutError:
      logger.info("no microphone audio in timeout")


  current_window.removeControl( mic_image )




def speech_run():
  ListRec = ListenRecognizer()

  while not monitor.abortRequested():
    # Sleep/wait for abort for 10 seconds
    if monitor.waitForAbort(1):
      # Abort was requested while waiting. We should exit
      break

    logger.debug("start listening for 'kodi' word! %s" % time.time())

    try:

      listened = ListRec.listen_and_recognize(1)

      found_activation_words = ListRec.check_words( listened, __ACTIVATION_WORD__ )

      if ( found_activation_words ):
        logger.info("found activation words")

        start_listening_for_command()

      else:
        logger.info( "No activation words found in {}".format(listened) )


    except WaitTimeoutError:
      logger.info("no microphone audio in timeout")



def run():
  logger.info(sys.path)
  from resources.lib import speech
  speech_run()
